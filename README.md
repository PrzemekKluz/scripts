TV-MANAG
========

Overview
--------

This is a documentation for TV-MANAG project which serves as video distributor for TVs connected to the network.
The purpose of this system is to be able to connect pendrive to the mother Raspberry Pi (RBPi) which will automatically 
send and turn on videos on all other RBPis connected to the network.

Infrastructure
--------------

The whole system is inside separated (not connected to the internet) network.

Two routers are located inside A1 server room in KRK office. 
They are marked with 'Innovation Lab' stickers.

One RBPi (master) is located on the reception desk, all other RBPis (slaves)
are near the TVs on all the floors (1,2,3,4,5).

All RBPis are configured to NOT be able to connect to wireless network. Only cable connection
is allowed.

The only way to connect to the whole system and change anything manually is to go to the server room (or near it) and connect
to the network called **TV-MANAG** (ssid may be hidden) and use password: Sabre!@#$% (or via cable connected to one of the routers).

Main router is configured on 192.168.1.1 and the login password pair for it is: admin/Sabre12345

Pendrive
--------

All files should be named **WITHOUT** white spaces.

ABC_DEF.mp4 <-- allowed
ABC DEF.mp4 <-- not allowed

All files which are to be played, need to be put inside **/tv_videos** directory

LEDs on master device's cover
-----------------------------

Master RBPi is equipped with two LEDs: green and red.
    * Glowing green LED indicates: 'Ready and waiting for pendrive insertion'
    * Glowing red LED indicates: 'Copying in progress. Don't remove pendrive'
    * Both LEDs turned off indicates: 'Process has finished. Please remove pendrive'
    
Shortly after the removal of the pendrive, green LED should glow again.

Single RBPi's configuration (slave)
-----------------------------------

The goal here was, to be able to connect new slaves quickly and without much additional configuration, so it's just plain LibreELEC image (**LibreELEC-RPi2.arm-9.0.1.img**).
OpenELEC does not work on RBPi 3B+ for the time being. 

All that needs to be done to connect new slave to the network is:
    
    1) Via LibreELEC's configuration
        * Enable SSH
        * Enable SAMBA
        * Enable remote connection from OTHER devices
        * Enable wired connection
        * Disable wireless connection
    2) Via SSH
        * Connect from master to slave via ssh to add it's ssh key
        * Copy /storage/.ssh/known_hosts from master to slave

Single RBPi's configuration (master)
------------------------------------

OpenELEC was chosen to run on master RBPi just because it has got an option to automatically mount pendrive (lame, I know).

First configuration of master:

    OpenELEC's configuration on master has to be the same as on slave (see section above).
    All slaves' ssh keys need to be put in /storage/.ssh/known_hosts on master RBPi

    Directory **scripts** and it's contents from this repo has to be put inside **/storage** directory and crontab has to be set to:

    ~~~
    */2 * * * * /storage/scripts/rebooter.sh &
    ~~~

    File /storage/.config/autostart.sh has to be created with it's contents:
        ~~~
        #!/bin/bash

        #ifconfig wlan0 down
        eval '/storage/scripts/leds/ledGreenOn.sh'
        eval '/storage/scripts/watcher.sh' >> /storage/logs/logs.log &
        ~~~

Code overview
-------------

    1) ipLookup.sh
        Lists devices' IP addrresses in the network
    
    2) myIp.sh
        Returns IP address of master device
        
    3) rebooter.sh
        Iterates through LibreELEC devices in the network with 10s ping to LibreELEC's json RPC API. If the API does not respond, it reboots the whole device if possible
        
    4) watcher.sh
        Starts copyFiles.sh every 2s
        
    5) copyFiles.sh
        Places an indicator on pendrive names /tv_videos/.check, to know if the pendrive had been removed and it has to copy new movies, or if the pendvive was left connected to the device.
        Copies new movies to /storage/tv_videos on master and runs startAll.sh
        
    6) startAll.sh
        * Iterates throuh all 'LibreELEC' devices in the network
            * Stops the player
            * Clears the playlist
            * Removes all movies from /storage/videos on slave device
            * Copies new movies to /storage/videos on slave device
            * Adds new movies to LibreELEC playlist of id=1
            * Starts the player with playlist 1
            
     All other scripts placed in /storage/scripts are legacy scripts or my tests and are no longer needed.
           
Logs
----

Scripts log their activity in **/storage/logs/logs.log**

Known issues
------------

1) Sometimes Kodi on LibreElec stops responding, that's why **rebooter.sh** script has been introduced.
2) Sometimes Kodi does not recognize proper path while adding files to playlist. That's why startAll.sh script has logic starting with line **33: while [ ! -z "$INVALID_RESULT" ]**.
3) Sometimes Kodi does not recognize new files. That's why every time a new file is being added to the playlist there are 10 retries with 3s sleep of this step in startAll.sh script.
4) LibreElec LibreELEC-RPi2.arm-8.2.5.img crashes randomly and cannot restart with Kernel Panic at startup. That's why all slaves uses LibreELEC-RPi2.arm-9.0.1.img now. Documentation of v9.0.1 claims this issue was fixed.
