#!/bin/bash

function scan ()
{
    for ip in $(seq 254); do
        ip=$1.$ip
        echo $ip
        ping -c 1 -W 1 $ip &
   done | sed -nE 's:^.* from ([0-9.]+).*time=(.*s)$:\1 (\2):p' | sed 's|(.*)||g'
}

if [ $1 ]; then
    scan $1
else
    scan 192.168.1
fi
