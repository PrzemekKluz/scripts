#!/bin/bash

echo "[$(date)] STARTED LOOKING FOR DEVICES IN NETWORK"
MY_IP=$(/storage/scripts/myIp.sh)

for ip in $(/storage/scripts/ipLookup.sh); do

	NETWORK_NAME=$(ssh -i /storage/.ssh/key -t root@$ip 'hostname' || echo "Cannot get hostname from ${ip}")
	HOSTNAME="$(echo -e "${NETWORK_NAME}" | tr -d '[:space:]')"
	LIBREELEC="LibreELEC"
	
	if [ ! "$ip" == "$MY_IP" ]; then
		if [ "$HOSTNAME" == "$LIBREELEC" ]; then
			echo "[$(date)] STOP THE PLAYER ON ${ip}"
		        #Stop the player
		        curl -D -  -H 'Content-Type: application/json' -d '{"jsonrpc":"2.0","id":"1","method":"Player.Stop","params":{"playerid": 1}}' http://$ip:8080/jsonrpc
			curl -D -  -H 'Content-Type: application/json' -d '{"jsonrpc":"2.0","id":1,"method":"Playlist.Clear","params":{"playlistid":1}}' http://$ip:8080/jsonrpc	
	        	echo ""
	
		        #Copy files
			echo "[$(date)] SSH: Removing old videos from ${ip}"
			ssh -i /storage/.ssh/key -t root@$ip 'mkdir -p /storage/videos' || echo "Cannot create directory on ${ip}"
		        ssh -i /storage/.ssh/key -t root@$ip 'rm -r /storage/videos' || echo "Cannot remove old movies from ${ip}"
			ssh -i /storage/.ssh/key -t root@$ip 'mkdir -p /storage/videos' || echo "Cannot create new directory on ${ip}"

			echo "[$(date)] SCP: Copying new videos to ${ip}"
			
			for file in $(ls /storage/tv_videos); do
			        scp -i /storage/.ssh/key /storage/tv_videos/$file root@$ip:/storage/videos/$file || echo "Cannot connect to ${ip}"
				sleep 3
					it=10
					INVALID_RESULT='Invalid'
					while [ ! -z "$INVALID_RESULT" ] 
					do
						CURL_RESULT=$(curl -D -  -H 'Content-Type: application/json' -d "{\"jsonrpc\":\"2.0\",\"id\":1,\"method\":\"Playlist.Add\",\"params\":{\"playlistid\":1,\"item\":{\"file\":\"/storage/videos/$file\"}}}" http://$ip:8080/jsonrpc)
						INVALID_RESULT=$(echo $CURL_RESULT | grep Invalid)
						if [ ! -z "$INVALID_RESULT" ]; then
							echo 'Invalid params found. Trying other method...'
							CURL_RESULT=$(curl -D -  -H 'Content-Type: application/json' -d "{\"jsonrpc\":\"2.0\",\"id\":1,\"method\":\"Playlist.Add\",\"params\":{\"playlistid\":1,\"item\":{\"file\":\"storage/videos/$file\"}}}" http://$ip:8080/jsonrpc)
							INVALID_RESULT=$(echo $CURL_RESULT | grep Invalid)
						fi
						sleep 3
						it=$((it-1))
						if [ $it -lt 1 ]; then
							INVALIRD_RESULT=''
							echo 'Finished 10 retires of invalid params without success'
						fi
					done
			done
	
			echo "[$(date)] START THE PLAYER ON ${ip}"
		        #Start the player
		        #curl -D -  -H 'Content-Type: application/json' -d '{"jsonrpc":"2.0","id":"1","method":"Player.Open","params":{"item":{"directory":"/storage/videos/"}}}' http://$ip:8080/jsonrpc
		        echo ""
		        #Set infinite loop
		        #curl -D -  -H 'Content-Type: application/json' -d '{"jsonrpc": "2.0", "method": "Player.SetRepeat", "params": { "playerid": 1, "repeat": "all" }, "id": 1}' http://$ip:8080/jsonrpc
			curl -D -  -H 'Content-Type: application/json' -d '{"jsonrpc":"2.0","id":1,"method":"Player.Open","params":{"item":{"playlistid":1},"options":{"repeat":"all"}}}' http://$ip:8080/jsonrpc		        
			echo ""
		        #Hide the menu
		        #curl -D -  -H 'Content-Type: application/json' -d '{"jsonrpc":"2.0","method":"GUI.SetFullscreen","params":{"fullscreen":"toggle"},"id":"1"}' http://$ip:8080/jsonrpc
		        echo "[$(date)] FINISHED STARTING MOVIES ON ${ip}"
		fi
	fi
done
echo "[$(date) FINISHED STARTING MOVIES FOR ALL DEVICES IN THE NETWORK"
