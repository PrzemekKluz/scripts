#!/bin/bash

usb_rootdir="/var/media"
usb_dir_name="tv_videos"
pi_storage_path="/storage/videos"

function createStamp() {
        currentDate=$(date)
	echo $currentDate > /storage/.check
        if [ -d "$PROPER_USB_PATH" ]; then
                echo $currentDate > $PROPER_USB_PATH/.check
        fi
}

function copyFiles() {
	eval '/storage/scripts/leds/ledGreenOff.sh'
	eval '/storage/scripts/leds/ledRedOn.sh'

        echo "[$(date)] New movies found. Copying to mother station"
	rm /storage/videos/*
        for file in $(find ${PROPER_USB_PATH} -type f); do
                echo $file > tmp
                sed -i "s|$PROPER_USB_PATH|$pi_storage_path|g" ./tmp
		echo "TEST: $(cat ./tmp)
                COPY_TO=$(cat ./tmp)
                cp "$file" "$COPY_TO"
                echo "[$(date)] Copied: $file to $COPY_TO"
		rm tmp
        done
        echo "[$(date)] Done copying files to mother station"

        eval '/storage/scripts/startAll.sh'
	eval '/storage/scripts/leds/ledRedOff.sh'
	eval '/storage/scripts/leds/ledGreenOff.sh'
}

if [ "$SEMAPHORE" == "FALSE" ]; then

	export SEMAPHORE=TRUE

	#Does proper file exist on usb drive?
	PROPER_USB_PATH="$(find ${usb_rootdir} -type d | grep -w ${usb_dir_name})"
	#Copy all files from the usb drive
	if [ -d "$PROPER_USB_PATH" ]; then
	        if [ ! -f "$PROPER_USB_PATH"/.check ]; then
	                createStamp
	                copyFiles
	        else
			USB_CHECK=$(cat $PROPER_USB_PATH/.check)
			STORAGE_CHECK=$(cat /storage/.check)
			if [ ! "$USB_CHECK" == "$STORAGE_CHECK" ]; then
	                        createStamp
	                        copyFiles
			fi
	        fi
	else
		eval '/storage/scripts/leds/ledGreenOn.sh'
	        createStamp
	fi

	#eval '/storage/scripts/leds/ledRedOff.sh'
	#eval '/storage/scripts/leds/ledGreenOff.sh'

	export SEMAPHORE=FALSE
fi
