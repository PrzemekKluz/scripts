#!/bin/bash

MY_IP=$(/storage/scripts/myIp.sh)

for ip in $(/storage/scripts/ipLookup.sh); do

        NETWORK_NAME=$(ssh -i /storage/.ssh/key -t root@$ip 'hostname' || echo "Cannot get hostname from ${ip}")
        HOSTNAME="$(echo -e "${NETWORK_NAME}" | tr -d '[:space:]')"
        LIBREELEC="LibreELEC"

            if [ ! "$ip" == "$MY_IP" ]; then
                if [ "$HOSTNAME" == "$LIBREELEC" ]; then
                    CURL_RESULT=$(curl -D -  -H 'Content-Type: application/json' -m 10 http://$ip:8080/jsonrpc --stderr - )
                    INVALID_RESULT_1=$(echo $CURL_RESULT | grep Failed)
                    INVALID_RESULT_2=$(echo $CURL_RESULT | grep 'Operation timed out')
                    if [ ! -z "$INVALID_RESULT_1" ]; then
                        ssh -i /storage/.ssh/key -t root@$ip 'shutdown -r now'
                    fi
                    if [ ! -z "$INVALID_RESULT_2" ]; then
                        ssh -i /storage/.ssh/key -t root@$ip 'shutdown -r now'
                    fi
                fi
            fi

done
