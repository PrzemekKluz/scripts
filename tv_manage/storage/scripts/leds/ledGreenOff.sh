#!/bin/bash

if [ ! -d /sys/class/gpio/gpio19 ]; then
	echo "19" > /sys/class/gpio/export
	sleep 1
fi

echo "out" > /sys/class/gpio/gpio19/direction
echo "0" > /sys/class/gpio/gpio19/value
