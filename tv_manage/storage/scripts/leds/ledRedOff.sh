#!/bin/bash

if [ ! -d /sys/class/gpio/gpio26 ]; then
	echo "26" > /sys/class/gpio/export
	sleep 1
fi

echo "out" > /sys/class/gpio/gpio26/direction
echo "0" > /sys/class/gpio/gpio26/value
